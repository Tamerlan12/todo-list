import React from "react";
import "../Less/styles.css";
import { useSelector } from "react-redux";
import { useEffect } from "react";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { GETList } from "../redux/features/reducer";
import { PostList } from "../redux/features/reducer";
import { DeleteList } from "../redux/features/reducer";
import { ChangeList } from "../redux/features/reducer";

const Todo = () => {
  const list = useSelector((state) => state.List);

  const dispatch = useDispatch();

  const [inputDescription, setInputDescription] = useState("");
  const [inputHeader, setInputHeader] = useState("");
  const [inputData, setInputData] = useState("");
  const [inputImg, setInputImg] = useState("");
  const [imageURL, setImageURL] = useState("");
// Логика для прикрепления файла
  const fileReader = new FileReader();
  fileReader.onloadend = () => {
    setImageURL(fileReader.result);
  };
                                      
  const inputImage = (event) => {
    event.preventDefault();
    const file = event.target.files[0];
    setInputImg(file);
    fileReader.readAsDataURL(file);
  };
// Отслеживание значения из инпутов
  const inputHead = (e) => {
    setInputHeader(e.target.value);
  };

  const inputDesc = (e) => {
    setInputDescription(e.target.value);
  };

  const inputDate = (e) => {
    setInputData(e.target.value);
  };
// Функция для добавления дела
  const handleAdd = () => {
    setInputHeader("");
    setInputDescription("");
    setInputData("");
    setInputImg("");
    setImageURL("");
    dispatch(PostList(inputDescription, inputHeader, inputData, imageURL));
  };

  const handleListRemove = (id) => {
    dispatch(DeleteList(id));
  };

  const handleListChange = (id, completed) => {
    dispatch(ChangeList(id, completed));
  };

  useEffect(() => {
    dispatch(GETList());
  }, [dispatch]);

  return (
    <>
      <div className="Header">
        <div className="AddToDo">
          <div>
            <input
              value={inputHeader}
              onChange={(e) => {
                inputHead(e);
              }}
              className="AddHeader"
              type="text"
              placeholder="Введите заголовок..."
            />
          </div>
          <div>
            <input onChange={inputImage} className="AddImg" type="file" />
          </div>
          <div>
            <input
              value={inputDescription}
              onChange={(e) => {
                inputDesc(e);
              }}
              className="AddDescription"
              type="text"
              placeholder="Введите описание..."
            />
          </div>
          <div>
            <input
              value={inputData}
              onChange={(e) => {
                inputDate(e);
              }}
              className="AddData"
              type="date"
              placeholder="Введите текст..."
            />
          </div>
        </div>
        <button onClick={() => handleAdd()} className="Add">
          Добавить
        </button>
      </div>
      <main className="TodoBlock">
        {list.map((el, id) => {
          return (
            <main key={id}>
              <div className="checkbox">
                <input
                  type="checkbox"
                  onChange={() => handleListChange(el.id, el.completed)}
                  checked={el.completed}
                />   {/*Позволяет выбрать задачу выполненной или невыполненной*/}
              </div>
              <header>
                <h3>{el.Header}</h3>
              </header>
              <div className="block">
                <div className="data">{el.Data}</div>
                <div>{el.Description}</div>
                <div className="img">
                  <img src={el.image} alt="Фото" />
                </div>
                <button onClick={() => handleListRemove(el.id)}>✖</button> {/*Кнопка удаления*/}
              </div>
            </main>
          );
        })}
      </main>
    </>
  );
};
export default Todo;
