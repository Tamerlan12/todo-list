const initialState = {
  List: [],
  Loading: false,
  error: null,
  addLoading: false,
  removeLoading: false,
};

const dayjs = require("dayjs");
const date = dayjs();

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case "list/GET":
      return {
        ...state,
        List: action.payload,
        Loading: false,
      };
    case "list/GET/pending":
      return {
        ...state,
        Loading: true,
      };
    case "list/GET/reject":
      return {
        ...state,
        error: action.payload,
        Loading: false,
        addLoading: false,
      };
    case "list/POST":
      return {
        ...state,
        addLoading: false,
        List: [...state.payload, action.payload],
      };
    case "list/POST/Pending":
      return {
        ...state,
        addLoading: true,
      };
    case "list/Load/reject":
      return {
        ...state,
        error: action.payload,
        Loading: false,
        addLoading: false,
      };
    case "list/DELETE":
      return {
        ...state,
        List: state.List.filter(function (item, index) {
          if (item.id !== action.payload) {
            return item;
          }
        }),
        removeLoading: false,
      };
    case "list/DELETE/pending":
      return {
        ...state,
        removeLoading: true,
      };
    case "list/PATH":
      return {
        ...state,
        List: state.List.map((item) => {
          if (item.id === action.payload.id) {
            item.completed = !item.completed;
            return item;
          }
          return item;
        }),
      };
    default:
      return state;
  }
};

export const GETList = () => {
  return async (dispatch) => {
    dispatch({ type: "list/GET/pending" });
    try {
      let responce = await fetch(`http://localhost:3001/ListArr`);
      let content = await responce.json();
      dispatch({ type: "list/GET", payload: content });
    } catch (e) {
      dispatch({ type: "list/GET/reject", payload: e.message });
    }
  };
};

export const PostList = (header, description, data, Image, Completed) => {
  return async (dispatch) => {
    dispatch({ type: "list/POST/Pending" });
    let ListToAdd = {
      Header: header,
      Description: description,
      Data: data,
      image: Image,
      completed: date.isAfter(data),
    };
    try {
      const responce = await fetch(`http://localhost:3001/ListArr`, {
        method: "POST",
        headers: {
          "Content-type": "application/json",
        },
        body: JSON.stringify(ListToAdd),
      });
      const content = await responce.json();

      dispatch({ type: "list/POST", payload: content });
    } catch (e) {
      dispatch({ type: "list/Load/reject", payload: e.message });
    }
  };
};

export const DeleteList = (id) => {
  return async (dispatch) => {
    dispatch({ type: "list/DELETE/pending" });
    try {
      const responce = await fetch(`http://localhost:3001/ListArr/${id}`, {
        method: "DELETE",
      });
      dispatch({ type: "list/DELETE", payload: id });
    } catch (e) {
      dispatch({ type: "list/DELETE/pending", payload: e.message });
    }
  };
};

export const ChangeList = (id, complete) => {
  return async (dispatch) => {
    let ListToChange = {
      completed: !complete,
    };
    try {
      const responce = await fetch(`http://localhost:3001/ListArr/${id}`, {
        method: "PATCH",
        headers: {
          "Content-type": "application/json",
        },
        body: JSON.stringify(ListToChange),
      });
      const content = await responce.json();
      dispatch({ type: "list/PATH", payload: { content, id } });
    } catch (e) {
      console.log(e.message);
    }
  };
};
